# Descargar

Puede descargar la ultima versión de login.py desde el [repositorio](https://bitbucket.org/haskmell/login.py/raw/f84d0140d37b19205b6ef57ff8bb987be74a3cec/login.py)


# Uso

Para usar el programa primero debe agregar su usuario y clave del mail USACH en
los campos correspondientes en login.py

Luego puede ejecutar login.py con: 

        python login.py
       
Si todo sale bien debería decir: "Entramos"


# Errores

En caso de encontrar un error, [reportalo](https://bitbucket.org/haskmell/login.py/issues?status=new&status=open) en español.



# Desarrollo

El desarrollo principal de este repositorio se encuentra en:

[https://bitbucket.org/haskmell/login.py/overview](https://bitbucket.org/haskmell/login.py/overview)
        
Si desea contribuir con nuevas características o correcciones de errores, debe tener una cuenta
en bitbucket.org, hacer un fork (copia) del repositorio, realizar sus
modificaciones y luego hacer un "Pull Request" para que sus cambios sean
añadidos al repositorio principal.

Más información puede ser encontrada en el manual de bitbucket:

* [Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository)
* [Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Working+with+pull+requests)
